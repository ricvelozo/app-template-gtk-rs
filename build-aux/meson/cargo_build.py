#!/usr/bin/env python3

import sys
from os import environ
from os.path import join
from shutil import copy2 as copy
from subprocess import run, CalledProcessError

_, source_root, build_root, output, name, build_type, features = sys.argv
manifest = join(source_root, 'Cargo.toml')
cargo_build = ['cargo', 'build', '--manifest-path', manifest]

if build_type in {'release', 'debugoptimized'}:
    cargo_build += ['--profile', build_type]
    profile = target = build_type
else:
    profile = 'dev'
    target = 'debug'

if features:
    cargo_build += ['--features', features]

# Necessary to cache Cargo files in Flatpak
if 'FLATPAK_ID' in environ:
    if 'CARGO_HOME' not in environ:
        environ['CARGO_HOME'] = join(build_root, 'cargo-home')

    cargo_build += ['--target-dir', join(build_root, 'target')]
    target_dir = join(build_root, 'target')
else:
    target_dir = join(source_root, 'target')

try:
    print(f'Cargo profile: \033[1m{profile}\033[0m')
    run(cargo_build, check=True)
    copy(join(target_dir, target, name), output)
except CalledProcessError as err:
    sys.exit(err.returncode)

use gettextrs::*;
use gtk::{glib, prelude::*};

use application::Application;
use main_window::MainWindow;

mod application;
mod config;
mod main_window;

fn main() -> glib::ExitCode {
    bindtextdomain(config::PACKAGE, config::LOCALE_DIR).unwrap();
    textdomain(config::PACKAGE).unwrap();

    config::load_resources();

    let app = Application::new();
    app.run()
}

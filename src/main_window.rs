use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::*;
use gtk::{
    gio,
    glib::{self, clone},
};

use crate::Application;

mod imp {
    use super::*;
    use gtk::CompositeTemplate;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/br/com/ricardoveloso/AppTemplate/main_window.ui")]
    pub struct MainWindow {
        #[template_child]
        pub split_view: TemplateChild<adw::NavigationSplitView>,
        #[template_child]
        pub search_button: TemplateChild<gtk::ToggleButton>,
        #[template_child]
        pub workspace_list: TemplateChild<gtk::DropDown>,
        #[template_child]
        pub workspaces: TemplateChild<gtk::Notebook>,
        #[template_child]
        pub bookmark_list: TemplateChild<gtk::ListView>,
        #[template_child]
        pub network_list: TemplateChild<gtk::ListView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MainWindow {
        const NAME: &'static str = "MainWindow";
        type Type = super::MainWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MainWindow {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            #[cfg(feature = "beta")]
            obj.add_css_class("devel");

            let app_name = glib::application_name().expect("the application has no name");
            let app_title = String::from(app_name) + " - arquivo_1.xml";
            obj.set_title(Some(&app_title));

            // Actions
            obj.add_action_entries([
                gio::ActionEntry::builder("fullscreen")
                    .activate(clone!(
                        #[weak]
                        obj,
                        move |_, _, _| match obj.is_fullscreen() {
                            false => obj.fullscreen(),
                            true => obj.unfullscreen(),
                        }
                    ))
                    .build(),
                gio::ActionEntry::builder("new-project")
                    .activate(Self::Type::unimplemented)
                    .build(),
                gio::ActionEntry::builder("open")
                    .activate(Self::Type::unimplemented)
                    .build(),
                gio::ActionEntry::builder("save")
                    .activate(Self::Type::unimplemented)
                    .build(),
                gio::ActionEntry::builder("save-as")
                    .activate(Self::Type::unimplemented)
                    .build(),
                gio::ActionEntry::builder("search")
                    .activate(clone!(
                        #[weak(rename_to = imp)]
                        self,
                        move |_, _, _| {
                            imp.search_button.set_active(!imp.search_button.is_active());
                        }
                    ))
                    .build(),
            ]);

            // Settings
            let settings = Application::default().settings();

            let settings_actions = gio::SimpleActionGroup::new();
            let action = settings.create_action("color-scheme");
            settings_actions.add_action(&action);
            obj.insert_action_group("settings", Some(&settings_actions));

            settings.connect_changed(Some("color-scheme"), Application::check_color_scheme);
            Application::check_color_scheme(&settings, "");

            // Bookmark and network lists
            let bookmark_model = gtk::StringList::new(&[]);
            let network_model = gtk::StringList::new(&[]);
            for i in 1..=64 {
                bookmark_model.append(&format!("Favorito {i}"));
                network_model.append(&format!("Rede {i}"));
            }

            let bookmark_model = gtk::SingleSelection::new(Some(bookmark_model));
            self.bookmark_list.set_model(Some(&bookmark_model));

            let network_model = gtk::SingleSelection::new(Some(network_model));
            self.network_list.set_model(Some(&network_model));
        }
    }

    #[gtk::template_callbacks]
    impl MainWindow {
        #[template_callback]
        fn on_inspector_clicked(_: &gtk::Button) {
            gtk::Window::set_interactive_debugging(true);
        }

        #[template_callback]
        fn on_sidebar_lists_activate(&self) {
            self.split_view.set_show_content(true);
        }

        #[template_callback]
        fn on_workspace_list_notify_selected_item(
            &self,
            _: &glib::ParamSpec,
            workspace_list: &gtk::DropDown,
        ) {
            let page = workspace_list
                .selected_item()
                .and_downcast::<gtk::NotebookPage>()
                .expect("failed to cast to `gtk::NotebookPage`")
                .position() as u32;
            self.workspaces.set_current_page(Some(page));
        }
    }

    impl AdwApplicationWindowImpl for MainWindow {}
    impl ApplicationWindowImpl for MainWindow {}
    impl WindowImpl for MainWindow {}
    impl WidgetImpl for MainWindow {}
}

glib::wrapper! {
    pub struct MainWindow(ObjectSubclass<imp::MainWindow>)
        @extends adw::ApplicationWindow, gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Native, gtk::Root,
                    gtk::ShortcutManager, gio::ActionGroup, gio::ActionMap;
}

impl MainWindow {
    pub fn new(app: &Application) -> Self {
        glib::Object::builder()
            .property("application", &Some(app))
            .build()
    }

    fn unimplemented<O>(_o: &O, _action: &gio::SimpleAction, _parameter: Option<&glib::Variant>) {
        let app = gio::Application::default()
            .and_downcast::<gtk::Application>()
            .expect("failed to cast to `gtk::Application`");
        let active_window = app.active_window();
        let dialog = adw::AlertDialog::builder()
            .heading(&gettext("Under development"))
            .body(&gettext("This functionality will soon be available."))
            .build();
        dialog.add_response("close", "OK");
        dialog.present(active_window.as_ref());
    }
}

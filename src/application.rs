use adw::prelude::*;
use adw::subclass::prelude::*;
use gettextrs::*;
use gtk::{
    gio,
    glib::{self, clone, WeakRef},
};

use crate::{config, MainWindow};

mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct Application {
        pub window: WeakRef<MainWindow>,
        pub settings: gio::Settings,
    }

    impl Default for Application {
        fn default() -> Self {
            Self {
                window: Default::default(),
                settings: gio::Settings::new("br.com.ricardoveloso.AppTemplate"),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    impl ObjectImpl for Application {}

    impl ApplicationImpl for Application {
        fn startup(&self) {
            self.parent_startup();
            let obj = self.obj();

            glib::set_application_name(&gettext("App Template"));

            obj.set_accels_for_action("app.help", &["F1"]);
            obj.set_accels_for_action("app.preferences", &["<Control>comma"]);
            obj.set_accels_for_action("app.quit", &["<Control>Q", "<Control>W"]);
            obj.set_accels_for_action("app.show-help-overlay", &["<Control>question"]);
            obj.set_accels_for_action("win.fullscreen", &["F11"]);
            obj.set_accels_for_action("win.new-project", &["<Control>N"]);
            obj.set_accels_for_action("win.open", &["<Control>O"]);
            obj.set_accels_for_action("win.save", &["<Control>S"]);
            obj.set_accels_for_action("win.save-as", &["<Control><Shift>S"]);
            obj.set_accels_for_action("win.search", &["<Control>F"]);

            let action = gio::SimpleAction::new("quit", None);
            action.connect_activate(clone!(
                #[weak]
                obj,
                move |_, _| obj.quit()
            ));
            obj.add_action(&action);
        }

        fn activate(&self) {
            let obj = self.obj();
            match obj.active_window() {
                None => MainWindow::new(&obj).present(),
                Some(win) => win.present(),
            };
        }
    }

    impl AdwApplicationImpl for Application {}
    impl GtkApplicationImpl for Application {}
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends adw::Application, gtk::Application, gio::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Application {
    pub fn new() -> Self {
        glib::Object::builder()
            .property("application-id", &Some(config::APP_ID))
            .property("flags", &gio::ApplicationFlags::default())
            .property(
                "resource-base-path",
                &Some("/br/com/ricardoveloso/AppTemplate/"),
            )
            .build()
    }

    pub fn settings(&self) -> gio::Settings {
        self.imp().settings.clone()
    }

    pub fn check_color_scheme(settings: &gio::Settings, _: &str) {
        let style_manager = adw::StyleManager::default();
        style_manager.set_color_scheme(match settings.string("color-scheme").as_str() {
            "light" => adw::ColorScheme::ForceLight,
            "dark" => adw::ColorScheme::ForceDark,
            _ => adw::ColorScheme::Default,
        });
    }
}

impl Default for Application {
    fn default() -> Self {
        gio::Application::default()
            .and_downcast::<Application>()
            .expect("failed to cast to `Application`")
    }
}
